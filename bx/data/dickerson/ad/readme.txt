adsig.annot adsig annotation with color lut
adsig.label all adsig individual labels combined into one singular labe
ag angular gyrus
ifs inferior frontal sulcus
itg inferior temporal gyrus
mtl medial temporal lobe
precun precuneus
sfg superior frontal gyrus
smg supramarginal gyrus
spl superior parietal lobule
tpole temporal pole