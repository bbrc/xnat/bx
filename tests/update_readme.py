#! /usr/bin/python

import os.path as op
import bx
from bx import parse
from bx import __version__


def update():
    """This script collects docstrings across the module, formats them and
    returns a string ready to be copy-pasted in the `README.md` file. Runs
    as it is with no arguments."""
    commands = parse.__find_all_commands__(bx)
    commands_txt = {e.__name__.split('.')[-1].lower()[:-7]: e.__doc__
                    for e in commands if e.__name__ != 'Command'}
    commands = {e.__name__.split('.')[-1].lower()[:-7]: e
                for e in commands if e.__name__ != 'Command'}
    logo = open(op.join(op.dirname(bx.__file__), 'data', 'logo')).read()
    desc = '%s\n' % logo
    desc = desc + 'bx (v%s)\n\nExisting commands:\n' % __version__

    for e, v in commands_txt.items():
        i = int(len(str(e)) / 6)
        tabs = (3 - i) * '\t'
        v = '%s%s' % (tabs, v) if v is not None else ''

        c = commands[e](e, [], None, None)
        desc = desc + '\n### **`%s`**:'\
                      '\n\n%s' % (e.upper(), __stylize_help__(c.__doc__))
    return desc


def __stylize_help__(doc):
    ln = doc.split('\n')
    sep = ':\t\t'
    subcommands = [e.split(sep)[0].strip(' ') for e in ln if sep in e]
    msg = '%s\n%s' % (ln[0], '\n'.join(ln[1:]))
    for e in subcommands:
        msg = msg.replace(' %s:' % e, ' `%s`:' % e)
    i = ln.index([e for e in ln if 'Usage:' in e][0])
    msg = msg.replace(ln[i+1], ln[i+1])  # useless but keeping it for the future maybe
    return msg


if __name__ == '__main__':
    readme = update()
    print(readme)
