import pyxnat
import json
from tqdm import tqdm
import pandas as pd
import os.path as op
import numpy as np
import argparse

config_file = '/home/grg/.xnat_bsc.cfg'
resource_name = 'SPM12_SEGMENT'
sd = '/home/grg/git/snaprate/web/data'
if_fp = '/home/grg/git/notebooks/QualityControl/IncidentalFindings/data/IF_MRI_QC_20191209.xlsx'


def find_hallazgo(e, hallazgos, incid):
    'Finds a subject in Oriol\'s table and return IF if any'

    # Categories (binary columns)
    columns = ['VASCULAR MALFORMATION / DEVELOPMENTAL ANOMALY  (1=present)',
               'VENTRICULAR SYSTEM ANOMALY (1=present)',
               'HIPPOCAMPAL MALROTATION (1=present)',
               'CYST (1=present)',
               'MENINGIOMA (1=present)',
               'POSSIBLE DEMYELINATING DISEASE (1=present)',
               'CALLOSUM BODY LESION (1=present)',
               'INFARCT (LARGE VESSEL) (1=present)',
               'INFARCT (SMALL VESSEL) (1=present)',
               'HEMORRAGIC LESION (1=present)']

    subject_label = e['subject_label']
    project_id = e['project']
    eid = e['ID']
    d = {'ALFA_OPCIONAL': 'AlfaOpcional',
         'ALFA_PLUS': 'Alfa+'}
    index = (d[project_id], int(subject_label))
    if index in hallazgos.index:
        t = []
        for c in columns:
            if incid.loc[index][c] == 1:
                t.append(c.split('(1=')[0].rstrip(' '))
        return [hallazgos.loc[index], ', '.join(t)]
    else:
        return None


def build_table(config_file, sd, resource_name, if_fp):
    """ Build a snaprate-compatible Excel table.

    Args:
    - config_file: XNAT configuration file
    - sd: Path to snaprate data
    - resource_name: Resource name (should be a folder in snaprate)
    - if_fp: Path to Oriol's Excel table with incidental findings
    """

    x = pyxnat.Interface(config=config_file)

    validator_names = {'SPM12_SEGMENT': 'SPM12SegmentValidator',
                       'ASHS': 'ASHSValidator',
                       'FREESURFER6_HIRES_aparc': 'FreeSurferHiresValidator',
                       'FREESURFER6_HIRES_aseg': 'FreeSurferHiresValidator'}

    test_names = {'SPM12_SEGMENT': ['HasNormalSPM12Volumes',
                                    'SPM12SegmentExecutionTime'],
                  'ASHS': ['HasNormalSubfieldVolumes',
                           'AreCAVolumesConsistent',
                           'HasAllSubfields'],
                  'FREESURFER6_HIRES_aparc': ['HasAbnormalAsegFeatures'],
                  'FREESURFER6_HIRES_aseg': ['HasAbnormalAsegFeatures']}

    # Collect subjects
    fp = op.join(sd, resource_name, 'subjects.json')
    if op.isfile(fp):
        subjects = json.load(open(fp))
    else:
        from glob import glob
        wd = op.join(sd, resource_name, 'snapshots', '*.jpg')
        subjects = [op.basename(e).split('.')[0] for e in glob(wd)]
        ans = input('%s not found: do you want to create it (%s subjects)? (y/N)'
                    % (fp, len(subjects)))
        if ans in 'yY':
            json.dump(subjects, open(fp, 'w'))
        else:
            print('Exiting.')
            import sys
            sys.exit(0)

    # Collect tests
    tests = []
    for s in tqdm(subjects[:]):
        row = [s]
        r = x.select.experiment(s).resource('BBRC_VALIDATOR')
        if r.exists():
            vn = validator_names[resource_name]
            res = r.tests(vn)
            for t in test_names[resource_name]:
                row.append(bool(res[t]['has_passed']))
            tests.append(row)
        else:
            print('Missing resource', s)

    columns = ['ID']
    columns.extend(test_names[resource_name])
    df = pd.DataFrame(tests, columns=columns).set_index('ID')

    fp = op.join(sd, resource_name, '%s.xls' % resource_name)
    df.to_excel(fp)

    # Take all experiments from XNAT
    experiments = x.array.experiments(project_id='ALFA_OPCIONAL',
                                      columns=['subject_label']).data
    experiments.extend(x.array.experiments(project_id='ALFA_PLUS',
                                           columns=['subject_label']).data)

    # Take all incidental findings from Oriol
    incid = pd.read_excel(if_fp, converters={'ID': np.int})
    incid = incid.set_index(['AlfaOpcional', 'ID'])

    columns = ['VASCULAR MALFORMATION / DEVELOPMENTAL ANOMALY  (1=present)',
               'VASCULAR MALFORMATION / DEVELOPMENTAL ANOMALY  (location)',
               'VENTRICULAR SYSTEM ANOMALY (1=present)',
               'HIPPOCAMPAL MALROTATION (1=present)', 'CYST (1=present)',
               'CYST (location)', 'MENINGIOMA (1=present)',
               'MENINGIOMA (location)',
               'POSSIBLE DEMYELINATING DISEASE (1=present)',
               'CALLOSUM BODY LESION (1=present)',
               'INFARCT (LARGE VESSEL) (1=present)',
               'INFARCT (LARGE VESSEL) (location)',
               'INFARCT (SMALL VESSEL) (1=present)',
               'INFARCT (SMALL VESSEL) (location)',
               'HEMORRAGIC LESION (1=present)',
               'HEMORRAGIC LESION (location)',
               'OTHER (description if present)']

    hallazgos = incid[['Hallazgo BBRC']]
    hallazgos.dropna()
    print(len(hallazgos), 'findings found in Oriol\'s table')

    # Iterate over all experiments
    table = []

    for e in experiments[:]:
        i = find_hallazgo(e, hallazgos, incid)
        if i is not None:
            row = [e['ID']]
            row.append(False)
            row.append(i[0]['Hallazgo BBRC'])
            row.append(i[1])
            table.append(row)
    columns = ['ID', 'HasNoFinding', 'FindingDesc', 'FindingCategory']
    inc = pd.DataFrame(table, columns=columns).set_index('ID')

    fp = op.join(sd, resource_name, '%s.xls' % resource_name)
    ashs = pd.read_excel(fp).set_index('ID')

    df3 = ashs.join(inc)
    df3['FindingDesc'].replace(np.nan, 'None', regex=True, inplace=True)
    df3['HasNoFinding'].replace(np.nan, True, regex=True, inplace=True)
    df3['FindingCategory'].replace(np.nan, 'None', regex=True, inplace=True)
    df3['FindingCategory'].replace('', 'None', regex=True, inplace=True)

    df3.to_excel(fp)
    print('Saved in %s' % fp)


def dir_path(s):
    if op.isdir(s):
        return s
    else:
        raise NotADirectoryError(s)


def create_parser():
    desc = 'Build a snaprate-compatible Excel table for a given resource'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('--config', '-c', help='XNAT configuration file',
                        type=argparse.FileType('r'), required=True)
    msg = 'Resource name (should be name an existing folder in snaprate)'
    parser.add_argument('--resource', '-r', required=True, help=msg)
    parser.add_argument('--sd', '-d', required=False,
                        default=sd, type=dir_path,
                        help='Path to snaprate data')
    parser.add_argument('--if_fp', '-i', required=False,
                        type=argparse.FileType('r'),
                        default=if_fp,
                        help='Oriol\'s table with incidental findings')
    parser.add_argument('--verbose', '-V', action='store_true', default=False,
                        help='Display verbosal information (optional)',
                        required=False)
    return parser


if __name__ == "__main__":
    parser = create_parser()
    args = parser.parse_args()
    try:
        sd = getattr(args.sd, 'name')
    except AttributeError:
        sd = args.sd
    try:
        if_fp = getattr(args.if_fp, 'name')
    except AttributeError:
        if_fp = args.if_fp
    build_table(args.config.name, sd, args.resource, if_fp)
